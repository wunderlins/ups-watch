# RaspberryPi UPS Daemon

<img src="assets/icon.png" align="left"> This is a simple deamon which watches a gpio pin's falling edge interrupt. If the pin does not receive power for N seconds it will run a configurable command (such as `halt`).

Typically one would attach a re-chargable battery between the powersupply and the RaspberryPi's power input and monitor the power input to that battery. If it goes low, trigger a shutdown after a configurable time.

This Project builds heavily on Lars Kellogg-Stedman's excellent [gpio-watch][1] project which was a great source of examples.

(c) 2020, Simon Wunderlin <swunderlin@gmail.com>, GPL 2

## Hardware Stup

### UPS 

An example UPS hardware setup:

```
BATTERY CHARGER
INPUT  ---> 220 kΩ --+--> 330 kΩ ------> GND
(5-5.3V)             |
                     v
          GPIO PIN 26 (3.0-3.28V)
```

I am using a Battery Charger / Boost Converter from [Aliexpress][2]. Li-Ion batteries are able to provide about 2C current, so with 2 * 2.5Ah @ ~3.5V 18650 Batteries (1S2P) this shield is able to provide ~17Wh @ up to 10A peak. This is more than enough for a controlled shutdown on power fail (or if someone delibarately cut the power and want the pi just to handle the graceful shutdown itself).

[![18650 Lithium Battery Shield V8 Mobile Power Expansion Board Module 5V/3A 3V/1A](assets/Lithium-Battery-Shield-small.jpg)][3]

### Power off Button

This is a way to trigger shutdown with a button, it basically inverts the ups functionality. When the power goes up on a pin, a shutdown is triggered.

```
RasPI                BUTTON     RASPI
3V3  ---> 220 kΩ ----> ./. ----> GND
```
Set `inverted` in `ups-watch.ini` to 1.

## Installation

### From source

You may want to build the code from source. For that you need to have `gcc` and `make` installed.

```sh
cd src
make
```

### Binary release 

Or you may use the pre-compiled binary `ups-watch`. It is compiled for Linux armv7l on Debian Buster (4.19.97-v7+). 

### Configuration

You will have to make sure the `ups-watch.ini` file is i nthe same folder and is readable by the executable. Have a look into the config file, you might have t ochange the pin number to whatever you want to use (default is 26, which is equivalent to FPIO26 in the below chart for Pi B+):

For a detaild description of the configuration options see [ups-watch.ini](ups-watch.ini).

![GPIO Pin Description](assets/Raspberry-Pi-GPIO-pinouts.png)

### Run as systemd service

if you want to use a different pin than pin 26 (Broadcom pin numbering) then change the it in the configuration file `ups-watch.ini`. Also `grace_period` and `shutdown_command` can be configured there.

### Service installation:

```sh
sudo ln -s /home/pi/ups-watch/ups-watch.service /etc/systemd/system/ups-watch.service
sudo systemctl enable ups-watch.service
```

### Start the service 

```sh
sudo systemctl start ups-watch.service
```

### Service configuration changes

if you change anything in `ups-watch.service` you'll have to tell systemd to reload the configuration:

```sh
systemctl daemon-reload
```

### ups-watch.ini config changes

You must restart the service if you change anything in the config file to take affect:

```sh
systemctl restart ups-watch.service
```

### Logging

All output (`stderr` and `stdout`) is sent to `ups-watch.log`.


## Run interactively

```sh
./ups-watch -i
```

This will not daemonize the program. All output is sent to `stdout` and `stderr` instead of the log file.


## Interrupt handler

Whenever the daemon process is running it is polling the configured pin. If the pin goes low and the grace period is reached, `shutdown_command` will be run. This command is started in `/bin/sh` and therefore can be anything you want it to be. Note: the command will be run with the service user you have configured in `ups-watch.service`.

[1]: https://blog.oddbit.com/post/2014-07-26-gpiowatch-run-scripts-in-respo/
[2]: https://www.aliexpress.com/item/4000834447648.html?spm=a2g0s.9042311.0.0.77ab4c4d4eEsWV
[3]: assets/Lithium-Battery-Shield.jpg