/**
 * RaspberryPI UPS Watchog
 * 
 * This program uses a gpio pin to watch if the raspberry pi ups 
 * receives power. If down, then after a grace period a shutdown 
 * command will be triggered.
 * 
 * Some parts were taken from: https://github.com/larsks/gpio-watch 
 * 
 * 2020, Simon Wunderlin <swunderlin@gmail.com>
 * 2014 Lars Kellogg-Stedman <lars@oddbit.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <poll.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>

#include "logging.h"
#include "gpio.h"

// globals from ups-watch.c
extern struct timespec ts;
extern volatile int keep_running;
extern pthread_mutex_t ts_mutex;
extern pthread_mutex_t keep_running_mutex;
extern int inverted;
extern volatile int stop;

// Return 1 if path is a directory, 0 otherwise.
int is_dir (const char *path) {
	struct stat buf;
	int err;

	err = stat(path, &buf);
	if (-1 == err) {
		return 0;
	}

	return S_ISDIR(buf.st_mode);
}

// Export a pin by writing to /sys/class/gpio/export.
void pin_export(int pin) {
	char *export_path,
	     *pin_path;
	int export_path_len,
	    pin_path_len;

	LOG_INFO("pin %d: exporting", pin);

	export_path_len = strlen(GPIO_BASE) + strlen("export") + 3;
	export_path = (char *)malloc(export_path_len);
	snprintf(export_path, export_path_len-1, "%s/export", GPIO_BASE);

	pin_path_len = strlen(GPIO_BASE) + GPIODIRLEN + 3;
	pin_path = (char *)malloc(pin_path_len);
	snprintf(pin_path, pin_path_len-1, "%s/gpio%d", GPIO_BASE, pin);

	if (! is_dir(pin_path)) {
		FILE *fp;
		//int tries = 0;

		fp = fopen(export_path, "w");
		if (!fp) {
			LOG_ERROR("pin %d: failed to open %s: cannot export",
				pin, export_path);
		}
		fprintf(fp, "%d\n", pin);
		fclose(fp);

		// gpio directories are initially owned by 'root'.  If you have
		// udev rules that change this, it may take a moment for those 
		// changes to happen.
		LOG_INFO("waiting for udev to catch up");
		sleep(1);
	}

//end:
	free(pin_path);
	free(export_path);
}

// Set which signal edges to detect.
int pin_set_edge(int pin) {
	char *pin_path;
	int pin_path_len;
	FILE *fp;

	LOG_INFO("pin %d: setting edge mode 'both'", pin);

	pin_path_len = strlen(GPIO_BASE) + GPIODIRLEN + strlen("edge") + 4;
	pin_path = (char *)malloc(pin_path_len);
	snprintf(pin_path, pin_path_len - 1,
			"%s/gpio%d", GPIO_BASE, pin);

	if (! is_dir(pin_path)) {
		LOG_ERROR("pin %d: not exported.", pin);
		exit(1);
	}

	snprintf(pin_path, pin_path_len,
			"%s/gpio%d/edge", GPIO_BASE, pin);

	fp = fopen(pin_path, "w");
	if (! fp) {
		LOG_ERROR("pin %d: failed to open %s: unable to set edge",
			pin, pin_path);
		exit(1);
	}

	fprintf(fp, "both\n");

	fclose(fp);
	free(pin_path);

	return 0;
}

// poll the pin and detect value changes
void poll_pin(int pin) {
  struct pollfd fdlist[1];
  int fd;
  int err;
  char buf[2] = {0};
  int val = 0;
  int last_val = -1;
  int value_path_len;
  char *value_path;
  
  // enable gpio
  pin_export(pin);

  // set both edges
  pin_set_edge(pin);

  // file to watch 
	value_path_len = strlen(GPIO_BASE) + strlen("value") + 9;
	value_path = (char *)malloc(value_path_len);
	snprintf(value_path, value_path_len, "%s/gpio%d/value", GPIO_BASE, pin);
  LOG_INFO("path: %s", value_path);

  // start polling
  fd = open(value_path, O_RDONLY);
  fdlist[0].fd = fd;
  fdlist[0].events = POLLPRI;

	// poll the pin's value indefinitely.
  while (1) {
    err = poll(fdlist, 1, -1);
    if (-1 == err) {
			// continue on signals, we need this otherwise we can't 
			// propperly handle signals in main
			if (errno == EINTR) {

				int rc = pthread_mutex_lock(&keep_running_mutex);
				if (rc) { /* an error has occurred */
						perror("pthread_mutex_lock");
						exit(4);
				}
				int kr = keep_running;
				rc = pthread_mutex_unlock(&keep_running_mutex);
				if (rc) {
						perror("pthread_mutex_unlock");
						exit(4);
				}

				if (kr)
      		continue;
				
				break; // we got SIGHUP or SIGINT, stop polling
			}
      perror("poll");
      exit(1);
    }

    // read value
    lseek(fdlist[0].fd, 0, SEEK_SET);
    err = read(fdlist[0].fd, buf, 1);
    val = atoi(buf);

		// invert value if inverted is configured
		if (inverted) {
			if (val == 1)
				stop = 1;
			// val = (val) ? 0 : 1;
		} else 

    // we are only interested in state changes,
    // this is also a poor way to debouncing
    if (val != last_val) {
      LOG_INFO("%d: v: %d, lv: %d, cont: %d", pin, val, last_val, keep_running);
      last_val = val;
			if (val == 0) {

				int rc = pthread_mutex_lock(&ts_mutex);
				if (rc) { /* an error has occurred */
						perror("pthread_mutex_lock");
						exit(4);
				}
				clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
				rc = pthread_mutex_unlock(&ts_mutex);
				if (rc) {
						perror("pthread_mutex_unlock");
						exit(4);
				}
				
			} else {
				ts.tv_sec  = 0;
				ts.tv_nsec = 0;
			}
    }

 }

  free(value_path);
}