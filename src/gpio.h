/**
 * RaspberryPI UPS Watchog
 * 
 * This program uses a gpio pin to watch if the raspberry pi ups 
 * receives power. If down, then after a grace period a shutdown 
 * command will be triggered.
 * 
 * Some parts were taken from: https://github.com/larsks/gpio-watch 
 * 
 * 2020, Simon Wunderlin <swunderlin@gmail.com>
 * 2014 Lars Kellogg-Stedman <lars@oddbit.com>
 */

#ifndef _GPIO_H
#define _GPIO_H

#define GPIODIRLEN 8
#ifndef GPIO_BASE
  #define GPIO_BASE "/sys/class/gpio"
#endif

void pin_export(int pin);
int pin_set_edge(int pin);
void poll_pin();

#endif // _GPIO_H