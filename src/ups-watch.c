/**
 * RaspberryPI UPS Watchog
 * 
 * This program uses a gpio pin to watch if the raspberry pi ups 
 * receives power. If down, then after a grace period a shutdown 
 * command will be triggered.
 * 
 * 2020, Simon Wunderlin
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <fcntl.h>
#include <libgen.h>
#include <pthread.h>

#include "ini.h"
#include "gpio.h"
#include "logging.h"

int pin = -1;
int grace_period = -1;
char *shutdown_command = NULL;
int inverted = 0;
int daemonize = 1;
char *pidfile;
struct timespec ts, cts;
volatile int keep_running = 1;
volatile int stop = 0;

pthread_mutex_t ts_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t keep_running_mutex = PTHREAD_MUTEX_INITIALIZER;

void sig_handler(int signo) {
	if (signo == SIGTERM || signo == SIGINT) {
		LOG_INFO("Caught SIGTERM, shutting down.");
		int rc = pthread_mutex_lock(&keep_running_mutex);
		if (rc) { /* an error has occurred */
				perror("pthread_mutex_lock");
				exit(4);
		}
		keep_running = 0;
		rc = pthread_mutex_unlock(&keep_running_mutex);
		if (rc) {
				perror("pthread_mutex_unlock");
				exit(4);
		}
	} else {
		LOG_WARN("Caught signal %d, don't know what to do.", signo);
		signal(signo, sig_handler);
	}
}

void register_signals() {
	if (signal(SIGTERM, sig_handler) == SIG_ERR)
		printf("\ncan't catch SIGTERM\n");
	if (signal(SIGINT, sig_handler) == SIG_ERR)
		printf("\ncan't catch SIGTERM\n");
	if (signal(SIGHUP, sig_handler) == SIG_ERR)
		printf("\ncan't catch SIGHUP\n");
	if (signal(SIGUSR1, sig_handler) == SIG_ERR)
		printf("\ncan't catch SIGUSR1\n");
	if (signal(SIGUSR2, sig_handler) == SIG_ERR)
		printf("\ncan't catch SIGUSR2\n");
}

void *check_command(void *data) {
	//int tid = *((int*)data);
	while (1) {
		
		int rc = pthread_mutex_lock(&ts_mutex);
		if (rc) { /* an error has occurred */
				perror("pthread_mutex_lock");
				exit(4);
		}

		if (ts.tv_sec > 0 || stop) {
			// current time 
			clock_gettime(CLOCK_MONOTONIC_RAW, &cts);
			if (cts.tv_sec-ts.tv_sec > grace_period || stop) {
				// we have reached the timeout, time to abort the mission
				LOG_WARN("We have reached %d timeout, running shutdown command", grace_period);
				LOG_WARN("%s", shutdown_command);

				// power could go down anytime now, make sure 
				// log is commited to disk
				sync();

				// run command
				int ret = system(shutdown_command);

				// check if we succeeded
				if (ret == -1 || ret == 127) {
					// we either failed to run the command or system() was unable
					// to fork a shell (127)
					LOG_WARN("Failed to run command '%s', system() returned %d",
										shutdown_command, ret);
					sync();
				}

				// we keep trying
				sleep(60);
			} /* else	{
				LOG_DEBUG("Ticking: %d", cts.tv_sec-ts.tv_sec);
			}
			*/
		}

		rc = pthread_mutex_unlock(&ts_mutex);
		if (rc) {
				perror("pthread_mutex_unlock");
				exit(4);
		}

		sleep(1);
	}
	pthread_exit(NULL);
}

/**
 * check if there is already a process running
 * 
 * we clean up the pid file if there is one but the process is not running.
 * 
 * returns 1 if it is running, 0 if there is no process running
 */
int is_running(char *pidfile) {
	int fd;
	char buf[10];
	int n, ret;

	fd = open(pidfile, O_RDONLY);
	if (fd >= 0) {
		n = read(fd, buf, sizeof(buf));
		close(fd);
		if (n) {
			n = atoi(buf);
			ret = kill(n, 0);
			if (ret >= 0) {
				fprintf(stderr,
						 "Daemon already running from pid %d\n", n);
				return 1;
			}
			fprintf(stderr,
					"Removing stale lock file %s from dead pid %d\n",
								 pidfile, n);
			unlink(pidfile);
		}
	}
	return 0;
}

int main(int argc, char *argv[]) {
	int errnum = 0;
	FILE *fp;

	// signal handling
	register_signals();

	// check if we shouuld run in interactive mode 
	// do not daemonize
	if (argc == 2 && strcmp(argv[1], "-i") == 0)
		daemonize = 0;

	// all files will be stored in the same directory as the executable.
	char exe[PATH_MAX];
	realpath(argv[0], exe);
	char *dir = dirname(exe);
	char dir_len = strlen(dir);
	char *inifile = malloc(sizeof(char) * (dir_len + 13));
	char *logfile = malloc(sizeof(char) * (dir_len + 13));
	pidfile = malloc(sizeof(char) * (dir_len + 13));
	snprintf(inifile, dir_len + 15, "%s/%s", dir, "ups-watch.ini");
	snprintf(logfile, dir_len + 15, "%s/%s", dir, "ups-watch.log");
	snprintf(pidfile, dir_len + 15, "%s/%s", dir, "ups-watch.pid");

	// check if there is already a process running?
	int running = is_running(pidfile);
	if (running)
		exit(0);

	// start logging
	loglevel = LEVEL_DEBUG; // debug logging == 3
	int fd;
	if (-1 == (fd = open(logfile, O_WRONLY|O_CREAT|O_APPEND, 0644))) {
		LOG_ERROR("failed to open logfile %s", logfile);
		exit(1);
	}

	// close stdout/stderr
	if (daemonize) {
		dup2(fd, 1);
		dup2(fd, 2);
	}

	// read configuration
	fp = fopen(inifile, "r");
	if (fp == NULL) {
		errnum = errno;
		LOG_ERROR("Error opening file: '%s', %d, %s", inifile, 
							errnum, strerror(errnum));
		
		return 2;
	} 
	
	ini_section_list_t *ini = ini_parse(fp);
	fclose(fp);

	if (ini == NULL) {
		LOG_ERROR("Failed to parse ini file: '%s', %d, %s", inifile,
							errnum, strerror(errnum));
		return 3;
	}

	// find pin
	char *cpin = ini_get_value(ini, "ups", "observe_pin");
	if (cpin == NULL) {
		LOG_ERROR("Failed to read 'observe_pin' value from ini file.");
		exit(2);
	}
	pin = atoi(cpin);

	// grace period config
	char *cgrace_period = ini_get_value(ini, "ups", "grace_period");
	if (cgrace_period == NULL) {
		LOG_ERROR("Failed to read 'grace_period' value from ini file.");
		exit(2);
	}
	grace_period = atoi(cgrace_period);

	// inverted command
	char *cinverted = ini_get_value(ini, "ups", "inverted");
	if (cpin == NULL) {
		LOG_ERROR("Failed to read 'inverted' value from ini file.");
		exit(2);
	}
	inverted = atoi(cinverted);
	if (inverted != 0) {
		inverted = 1;
		grace_period = 0;
	}

	char *cshutdown_command = ini_get_value(ini, "ups", "shutdown_command");
	if (cshutdown_command == NULL) {
		LOG_ERROR("Failed to read 'shutdown_command' value from ini file.");
		exit(2);
	}

	// read shutdown command
	int sc_len = strlen(cshutdown_command);
	shutdown_command = malloc(sc_len + 1);
	strncpy(shutdown_command, cshutdown_command, sc_len+1);

	// free momory from ini file, we don't need it anymore
	ini_free(ini);

	// daemonize the process
	if (daemonize)
		daemon(1, 1);

	// write pid to pid file
	int fd_pid;
	if (-1 == (fd_pid = open(pidfile, O_WRONLY|O_CREAT, 0644))) {
		LOG_ERROR("failed to open pidfile %s", pidfile);
		exit(1);
	}
	
	// write pid file
	char pidc[50] = {0};
	sprintf(pidc, "%d", getpid());
	write(fd_pid, pidc, strlen(pidc));
	close(fd_pid);

	// this thread checks periodically if we need to take action
	// it will check if the last bad state is older than grace_period
	ts.tv_sec = 0;
	ts.tv_nsec = 0;
	pthread_t	p_thread;
	int thr_id = pthread_create(&p_thread, NULL, check_command, (void*)NULL);

	// start polling the pin, this will enter an endless loop
	// the loop will be gracefully ended with SIGTERM or SIGINT
	poll_pin(pin);

	// clenaup
	close(fd);
	unlink(pidfile);
	free(pidfile);
	free(logfile);
	free(inifile);
	free(shutdown_command);
	pthread_cancel(thr_id);
	pthread_mutex_destroy(&ts_mutex);
	pthread_mutex_destroy(&keep_running_mutex);
	
	LOG_INFO("Done, bye bye.");

	exit(0);
}
